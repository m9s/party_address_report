#This file is part of Tryton.  The COPYRIGHT file at the top level of this
#repository contains the full copyright notices and license terms.
{
    'name': 'Party Address Report',
    'name_de_DE': 'Parteien Adressenlisten',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''
    - Adds an address report for parties
    - Provides a wizard for the search criteria "party address zip"
      and "party categories".
    ''',
    'description_de_DE': '''
    - Fügt den Bericht Adresslisten für Parteien hinzu.
    - Ermöglicht die Auswahl von Adressen anhand von Suchkriterien für
      Postleitzahlenbereiche und Parteikategorien.
    ''',
    'depends': [
        'ir',
        'res',
        'party',
    ],
    'xml':         [
        'party_address_report.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
